#include <iostream>
#include <string>
#include <memory>

#include "src/MessageSource.h"

/*
 * RAII wrapper that manages a MessageSource resource
 */
class MessageSourceWrapper {
public:
    explicit MessageSourceWrapper(const std::string& url) :
        src_(MessageSource::create()),
        url_(url) {
    }

    MessageSourceWrapper(const MessageSourceWrapper&) = delete;
    MessageSourceWrapper& operator=(const MessageSourceWrapper&) = delete;

    ~MessageSourceWrapper() {
        src_->disconnect();
        delete src_;
        std::cout << "MessageSource deleted by destructor" << std::endl;
    }

    std::string fetch() {
        if (not src_->connected()) {
            src_->connect(url_);
        }
        return src_->fetch();
    }
private:
    MessageSource* src_;
    const std::string url_;
};

std::string
fetchMessageUsingWrapper(const std::string& url) {
    MessageSourceWrapper source(url);
    return source.fetch();
}

/*
 * Using Dependency Injection to provide a source, relieving this
 * function of the responsibility to destroy source...
 */
std::string
fetchMessageFromSource(MessageSource* source, const std::string& url) {
    std::string message;

    try {
        source->connect(url);
        message = source->fetch();        
    }
    catch (...) {
        //we have to make sure to disconnect at all
        //points where we can exit the function...
        source->disconnect();
        throw;
    }

    source->disconnect();

    return message;
}



//Custom deleter function
void
messageSourceDeleter(MessageSource* source) {
    source->disconnect();
    delete source;
    std::cout << "MessageSource deleted by deleter function" << std::endl;
}

//Example using unique_ptr with custom deleter
std::string
fetchMessageSmartPointer(const std::string& url) {
    std::unique_ptr<MessageSource, decltype(&messageSourceDeleter)>
        source(MessageSource::create(), &messageSourceDeleter);
    source->connect(url);
    return source->fetch();
}

int main(int argc, char** argv) {

    MessageSource* source = MessageSource::create();
    const std::string message = fetchMessageFromSource(source, "my.message.source");
    std::cout << "Fetched a message: " << message << std::endl;
    delete source;


    const std::string message2 = fetchMessageUsingWrapper("wrapped.message.source");
    std::cout << "Fetched a message: " << message2 << std::endl;

    const std::string message3 = fetchMessageSmartPointer("my.message.source");
	std::cout << "Fetched a message: " << message3 << std::endl;
    return 0;
}
