#pragma once
#include <string>

class MessageSource {
public:
    static MessageSource* create();

    virtual ~MessageSource();

    virtual void connect(const std::string& url) = 0;
    virtual void disconnect() = 0;
    virtual bool connected() = 0;

    virtual std::string fetch() = 0;
};




