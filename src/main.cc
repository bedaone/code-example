#include <iostream>
#include <string>
#include "src/MessageSource.h"


class Log {
public:
    Log() :
        prefix_() {
    }

    Log(std::string prefix) :
        prefix_(prefix) {
    }

    void info(std::string message) {
        std::cout << prefix_ << " INFO: "
                  << message << std::endl;
    }

private:
    std::string prefix_;
};


std::string
fetchMessage(std::string url) {
    MessageSource* source = MessageSource::create();

    source->connect(url);
    std::string message = source->fetch();
    source->disconnect();

    return message;
}


int main(int argc, char** argv) {
    Log log();

    std::string message = fetchMessage("my.message.source");
    log.info(message);

    return 0;
}

