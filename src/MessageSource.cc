#include "src/MessageSource.h"

#include <stdexcept>

/*
 * Mock implementation of the MessageSource interface.
 */
class MockMessageSource : public MessageSource {
public:
    MockMessageSource() { }
    virtual ~MockMessageSource() { }

    void connect(const std::string& url) {
        url_ = url;
        connected_ = true;
    }

    void disconnect() {
        connected_ = false;
    }

    bool connected() {
        return connected_;
    }

    std::string fetch() {
        if (not connected_) {
            throw std::runtime_error("Not connected");
        }
        return "hello from " + url_;
    }

private:
    std::string url_;
    bool connected_;
};


MessageSource*
MessageSource::create() {
    return new MockMessageSource();
}

MessageSource::~MessageSource() { }
