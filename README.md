# Some example code to talk about...

## Building
Not really intended to be build, but the project can be compiled with bazel
`bazel build //src:all`

## What to talk about
While the code does (almost) compile and work, there are plenty of opportunity for improvement, both large and small. This is intentional, and should work as the basis for a discussion. While some editing could be made to the code during discussion, thats not really neccessary, focus should be on discussing improvements.

### main.cc
* a most vexing parse prevents the code from compiling
* `explicit` keyword for `Log(std::string)`
* `const&` arguments
* `const` member
* `const` variable

Main focus should be the `fetchMessage()` function (assume that the MessageSource can not be changed at this point):
* there is a memory leak right away, since souce is never deleted
* ...once the leak is found (and corrected), discuss possible exceptions that could occur?
* ...an exception could lead to another memory leak, how should this be handled?
* ...`disconnect()` would never be called either in case of exception
* (there are multiple ways to solve, including a wrapper class, using smart pointer with custom deleter, we are looking for RAII in one way or another)


### MessageSource.h
Not really the focus so far, most of the discussion should focus on main.cc, but there are a few questions that can be asked here too:
* Whats the meaning of `=0` after a virtual method? Whats that called (pure virtual)? What's a class with these kind of methods called? (abstract class)
* What is the purpose of `MessageSource::create`? (factory method, return interface type)
* Are there any improvements that can be made here? (`connected()` could be `const`, factory method could return a smart pointer type)


## Future improvements to this example code
* c++11 stuff
* async signal safe
* rule of three/five
